# Node_API
Node API with Express and Postgres

## Table of Contents

- [Prerequisites](#prerequisites)
- [Setup](#setup)
- [Notifications](#notifications)
- [Repository Variables](#repository-variables)
- [Application Variables](#application-variables)
- [Secrets](#secrets)
- [Cautions](#cautions)
- [SDLC](#sdlc)
- [Manifests](#manifests)
- [Reusability](#reusability)
- [Expanding](#expanding)

---


## Prerequisites

To use this project, the following prerequisites must be met:

- A Kubernetes cluster, such as minikube, that includes a GitLab Runner deployment.[Gitlab Runner documentation](https://docs.gitlab.com/runner/install/kubernetes.html)
- Separate dev and production environments. The dev environment runner should be configured to run jobs tagged as "dev", and the production environment runner should be configured to run jobs tagged as "production".


---


## Setup

To use this project, you should setup [Gitlab Runner documentation](https://minikube.sigs.k8s.io/docs/start/) locally. Install gitlab runner to minikube with following configuration(we should mount docker socket since e are going to use docker in docker for builds):
```   [[runners]]
      [runners.kubernetes]
        namespace = "default"
        image = "ubuntu:20.04"
        privileged = true
      [[runners.kubernetes.volumes.host_path]]
        name = "docker"
        mount_path = "/var/run/docker.sock"
        host_path = "/var/run/docker.sock"
        read_only = true
```
Please do not forget to add tag your gitlab runner. This will be help to decide which jobs gitlab runner will be run. So you can run only dev processes in dev environment and prod processes in prod clusters. Do not forget to create RBAC in kubernetes. You can do it with set rbac.create=true in helm chart values. Lastly do not forget set your gitlabUrl and runnerRegistrationToken in helm config. You can find them from repository config. For this project this image has been used for gitlab runner : gitlab/gitlab-runner:v13.0.0.


---


## Notifications

You can use webhooks for notifying on any issue on CI/CD process. First you can set your webhook address on target system. For slack you can find further information with [documentation](https://api.slack.com/messaging/webhooks). After that click settings -> webhooks on repo. And set necessary information about your notifications. You can send any type of event notification.

---

## Repository Variables

The following variables should be set(if not exist) in your Git repository:

- `CI_REGISTRY_USER`: The username for the GitLab registry.
- `CI_REGISTRY_PASSWORD`: The password for the GitLab registry.
- `CI_REGISTRY`: The URL of the GitLab registry.
- `CI_DEFAULT_BRANCH`: The default branch for the repository.
- `CI_COMMIT_BRANCH`: The current branch being built.
- `CI_COMMIT_TAG`: The current commit tag.
- `CI_COMMIT_SHORT_SHA`: The current commit short sha.
- `CI_REGISTRY_IMAGE`: The image name for the GitLab registry.

---

## Application Variables

You can refer environments field ./manifests/values.yaml file

---

## Secrets

You can refer secrets field ./manifests/values.yaml file. You should apply your secrets manually for security reasons. Visit the [Kubernetes documentation](https://kubernetes.io/docs/concepts/configuration/secret/) for more information on secrets.

---

## Cautions

The following precautions should be taken when using this project:

- Notify Webhooks: Ensure that you set your webhook URL correctly file to receive error notifications about the CD process.
- Repository Variables: Be sure to set the correct repository variables.
- Secrets: Ensure that the correct secrets are applied to the Kubernetes cluster.
- Runners: Be sure you configured your gitlab runners correctly. Visit the [Gitlab Runner documentation](https://docs.gitlab.com/runner/configuration/) for more information on Gitlab Runners. Also be sure about you set tags corretly for deploying images to true environment. 

---

## SDLC (Software Development Life Cycle)

The SDLC for this project is as follows:

1. The developer first develops the application in their local environment and pushes the code to the `dev` branch and creates a merge request. This merge request is reviewed by other team members or an authorized person. If there are no issues, the code is merged into `master`.
2. After the merge, the CI process for the `dev` environment starts, and a Docker image is built. This built image is then deployed to the `dev` environment using the Helm chart in the `manifests` folder.
3. To deploy the application to the production environment, a commit tag must be added to `master` by an authorized team member. This tag increases the version and deploys to the Kubernetes environment.


---

## Manifests

The manifest files are stored in the `manifests` folder and stored as a Helm chart. The most important part of this chart is the `values.yaml` file. Using this file, you can directly modify the application's configuration. For example, you can change the database host address or username directly with this file.


---

## Reusability

You can easily reuse this repository. Copy the gitlab-ci.yml file and manifests folder into your new repository, and you can reuse them as they are.Just please change name in the Charts.yaml file. Charts and pipelines are created with variables. This means that if you set the environment variables correctly for another application, you can use the manifests and gitlab-ci.yml files as they are. After copying, you can customize these files as desired.

---

## Expanding

You can easily create a new development, testing, or staging environment. If you want to open a common environment with the dev, just add a new tag to the deploy-dev step in the gitlab-ci.yml file. However, if you want to do something completely different from the dev environment (such as staging), you should create a staging branch. Then you should add a deploy-staging step that watches this branch. Sample configuration can be as follows:
```
deploy-staging:
  stage: deploy-staging
  image: alpine/helm:3.11.3
  dependencies:
    - docker-dev  
  script:
    - |
      RELEASE_NAME=$(echo "$CI_PROJECT_NAME" | tr '[:upper:]' '[:lower:]' | tr '_' '-')
      helm upgrade "$RELEASE_NAME" ./manifests \
        --install \
        --set image.repository="$CI_REGISTRY_IMAGE" \
        --set image.tag="$CI_COMMIT_SHORT_SHA" \
        --set NODE_ENV=staging
  only:
    - staging
  tags:
    - staging
```

---
